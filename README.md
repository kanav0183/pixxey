> firebase deploy --only hosting:noshoutin     
{
  "hosting": {
    "site":"noshoutin",
    "public": "build",
    "ignore": [
      "firebase.json",
      "**/.*",
      "**/node_modules/**"
    ],
    "rewrites": [
      {
        "source": "**",
        "destination": "/index.html"
      }
    ]
  }
}%  