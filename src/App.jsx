import React ,{useState,useEffect} from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from 'react-router-dom';
import './App.css'
import CommonNavSide from './Components/CommonBars/CommonNavSide'
import {AgentDashboard} from './Components/AgentDashboard/AgentDashboard'
import {TextSearchImage} from './Components/TextSearch/TextSearchImage'
import SignIn from './Components/SignIn/App'
import {LoaderCircles} from './Components/utils/utilComp'
import SpeechAnalysis from './Components/SpeechAnalysis/SpeechAnalysis'


const AppRoutes = ({isAuthenticated}) => {

  return (
    <>
        <Switch>
        <AuthenticatedRoute isAuthenticated = {isAuthenticated} path="/product">
                        <CommonNavSide>
                                <AgentDashboard />
                        </CommonNavSide>
        </AuthenticatedRoute>
       
        <AuthenticatedRoute isAuthenticated = {isAuthenticated} path="/speech">
                        <CommonNavSide>
                                <SpeechAnalysis />
                        </CommonNavSide>
        </AuthenticatedRoute>

        <AuthenticatedRoute isAuthenticated = {isAuthenticated} path="/text">
                        <CommonNavSide>
                                <TextSearchImage />
                        </CommonNavSide>
        </AuthenticatedRoute>

        <UnauthenticatedRoutes />
                 
        </Switch>
    </>
  );
};


const AuthenticatedRoute = ({ children, isAuthenticated , ...rest }) => {
  console.log(isAuthenticated ,"checking status in route for ********************")
  return (
    <Route
      {...rest}
      render={() =>
        isAuthenticated ? (
            <div>
          {children}
          </div>
        ) : (
          <Redirect to="/signin" />)}
    ></Route>
  );
};

const UnauthenticatedRoutes = () => (
  <Switch>
    <Route path="/signin">
        <SignIn />
    </Route>
    <Route exact path="/">
        <Redirect to="/orders" />
    </Route>
    <Route path="*">
        <Redirect to="/speech" />
    </Route>
  </Switch>
);





function App() {

  const [authTokenValid, setauthTokenValid] = useState(true)// change it to null , currently true to avoid logout
  useEffect(() => {
        // const token = localStorage.getItem('uid')
        
        // const authMainPageCheck = async () =>{
        //     await axios.post(tokenAuthCheckURL , {
        //                                           token:token,
        //       }).then(result => result.data ).then(
        //         result =>{
        //           if(result.status===200){
        //               console.log("Authentication Given ")
        //               setauthTokenValid(true)
        //             }
        //           else{
        //             console.log("Authentication Not Given ")
        //             setauthTokenValid(true) // change it to false , currently true to avoid logout
        //             }
        //         }
        //       )
        //   }
            // authMainPageCheck()
            console.log("Nothing happened")
            setauthTokenValid(true)
        }, []);

        if (authTokenValid === null)
            return (
              <LoaderCircles />
            ); 
        else
            return (
              <Router>
                <AppRoutes isAuthenticated = {authTokenValid}/>
              </Router>
            );
        }


export default App;
