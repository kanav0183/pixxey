import React, { useState } from 'react'
import 'react-dropzone-uploader/dist/styles.css'
import Dropzone from 'react-dropzone-uploader'
import { Image ,Card,Label,Message,Header,Icon} from 'semantic-ui-react'
import ModalExampleModal from './ModalTags'


const CardExampleCard = ({ OBJECT }) => {
    return(

        <Card>
        <Image src={OBJECT.url} wrapped ui={false} />
          <Card.Content>
              <Card.Meta>
              </Card.Meta>
              <Card.Description>
                  <Label.Group tag>
                      {
                          OBJECT.tags.split(" ").slice(0, 3).map((obj) =>
                                  <Label as='a' color='blue' >
                                      {obj}
                                  </Label>
                          )
                      }
                  </Label.Group>
              </Card.Description>
          </Card.Content>
              <Card.Content extra>
                  <ModalExampleModal objects= {OBJECT} />
                      
          </Card.Content>
      </Card>
  
    )
}
 
 

const AgentDashboard =()=>{
    const [random_words_array ,setrandom_words_array ]= useState([])
    const getUploadParams = async ({ file ,name }) => {
        const data = new FormData()
        data.append('file', file)
        data.append('filename', name)

        const response = await fetch('http://localhost:8000/compare_image', { method: 'POST', body: data })
        const data_json = await response.json()
        console.log(data_json)
        setrandom_words_array(data_json)

        return { url: 'https://httpbin.org/post', data }
      }
  
        const handleChangeStatus = ({ meta }, status) => {
        console.log(status, meta)
        }
    
        const handleSubmit = (files, allFiles) => {
        console.log(files.map(f => f.meta))
        allFiles.forEach(f => f.remove())
        }

        


        return (

            <div style={{backgroundColor:"#fafafa" ,position:"absolute" ,width:"95%"}}>
                <div className="addAppMainecontainer" style={{  marginTop:"5em",marginLeft:"1em",marginRight:"2em" }}>
                    <div className="androidApps" style={{height:"100%",width:"100%"}}>
                    
                        <Header as='h2'>
                            <Icon name='image' />
                            <Header.Content> Image Search</Header.Content>
                        </Header>
                            <Dropzone
                                getUploadParams={getUploadParams}
                                onChangeStatus={handleChangeStatus}
                                onSubmit={handleSubmit}
                                accept="image/*,audio/*,video/*"
                                inputContent={(files, extra) => (extra.reject ? 'Image, audio and video files only' : 'Drag Files')}
                                styles={{
                                dropzoneReject: { borderColor: 'red', backgroundColor: '#DAA' },
                                inputLabel: (files, extra) => (extra.reject ? { color: 'red' } : {}),
                                }}/>
                        <Message info>
                            <Message.Header>Image search </Message.Header>
                                <p> Upload image here and you ll get closest images to it in just few seconds.</p>
                        </Message>
                        <br></br>
                    </div>
                    <Card.Group itemsPerRow={4}>
                        {
                        random_words_array.map( (obj) =>
                            <CardExampleCard 
                                    OBJECT={obj}
                            />
                            )
                        }
                    </Card.Group>
                </div> 
            </div>
        );
      }


export  {AgentDashboard};

