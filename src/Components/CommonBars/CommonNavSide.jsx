
import React from 'react'
import MySidebar from './FixedSideBar'
import {Container,Button,Menu,Grid} from 'semantic-ui-react'
import firebase from 'firebase/app';
const onLogoutSuccess = () => {
  console.log("logout is clicked")
  localStorage.removeItem('uid')
  window.location.href='/'
  firebase.auth().signOut().then(() => {
    // Sign-out successful.
  }).catch((error) => {
    // An error happened.
  });
  window.location.href='/'
};
const CommonNavSide = ({children}) => (
  
  <div style={{backgroundColor:"#f2f2f2"}}>
    <Menu fixed='top' inverted>
      <Container>
        <Menu.Item as='a' header>
        </Menu.Item>
        <Menu.Menu position='right'>
         
        <Menu.Item>
        <Button as='a' onClick={onLogoutSuccess} style={{ marginLeft: '0.5em' }}>
                    Logout 
                    </Button>
          </Menu.Item>
        </Menu.Menu>
      </Container>
    </Menu>

        <div className="mainChildrenContainer" style={{marginTop:"1.5em"}}>
          <Grid>
            <Grid.Column width={3}>
                <MySidebar />
            </Grid.Column>
            <Grid.Column width={13}>
                {children}
            </Grid.Column>
          </Grid>
        </div>
      </div>
)
export default CommonNavSide
