import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Icon,Menu } from 'semantic-ui-react'

export default class MySidebar extends Component {
    state = {}
    handleItemClick = (e, { name }) => this.setState({ activeItem: name })
    componentDidMount() {}
    render() {
        const { activeItem } = this.state
        return(
                <Menu className='inverted vertical left fixed' style={{marginTop : "16px", width:"18.5%" }}>
                        <Link to="/home"><h2 style={{color:"white" , paddingLeft:"17px" }}>PixEy</h2></Link>
                            <Menu.Item></Menu.Item>
                            <Menu.Item name='speech' as ={Link} to='/speech'    active={activeItem === 'speech'} onClick={this.handleItemClick}>
                                <Icon.Group size='large' style={{padding:"10px"}}>
                                    <Icon name='file audio outline' />
                                    </Icon.Group>
                                    Smart Speech
                                </Menu.Item> 

                                <Menu.Item name='Image Search' as ={Link} to='/product'    active={activeItem === 'products'} onClick={this.handleItemClick}>
                                <Icon.Group size='large' style={{padding:"10px"}}>
                                    <Icon name='boxes' />
                                    </Icon.Group>
                                    Image Search
                                </Menu.Item> 


                                <Menu.Item name='Text' as ={Link} to='/text'    active={activeItem === 'Text'} onClick={this.handleItemClick}>
                                <Icon.Group size='large' style={{padding:"10px"}}>
                                    <Icon name='boxes' />
                                    </Icon.Group>
                                    Text Search
                                </Menu.Item> 


                </Menu>
        )
    }
}
