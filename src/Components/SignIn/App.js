import React, { Component } from 'react';
import firebase from 'firebase/app';
import 'firebase/auth';
// import firebaseConfig from './firebaseConfig';
import * as firebaseui from 'firebaseui'
import 'firebaseui/dist/firebaseui.css'
import SignedIn from './signed-in.js'


class SignIn extends Component {
  componentDidMount() {
    // const firebaseApp = firebase.initializeApp(firebaseConfig);

    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        // User is signed in, see docs for a list of available properties
        // https://firebase.google.com/docs/reference/js/firebase.User
        var uid = user.uid;
        console.log('sign in called')
        localStorage.setItem("uid",uid)
        // ...
      } else {
        uid=null
      }
      return uid
    });

    var uiConfig = {
      signInSuccessUrl: '/signed-in',
      signInOptions: [
        {
          provider: firebase.auth.PhoneAuthProvider.PROVIDER_ID,
          recaptchaParameters: {
            type: 'image', // 'audio'
            size: 'invisible', // 'invisible' or 'compact'
            badge: 'bottomleft' //' bottomright' or 'inline' applies to invisible.
          },
          defaultCountry: 'IN', 
          defaultNationNumber:'+91',
          defaultNationalNumber: '1234567890',
          whitelistedCountries: ['IN']
        }
      ],
    };

    // Initialize the FirebaseUI Widget using Firebase.
    var ui = new firebaseui.auth.AuthUI(firebase.auth());
    // The start method will wait until the DOM is loaded.
    ui.start('.firebaseui-auth-container', uiConfig);

    this.unregisterAuthObserver = firebase.auth().onAuthStateChanged(
        (user) => this.setState({isSignedIn: !!user})
    );
    // firebase.auth().signOut().then(() => {
    //   // Sign-out successful.
    // }).catch((error) => {
    //   // An error happened.
    // });
    
  }

  render() {
    return(
      <>
        {/* <div> <h3>Welcome to WinGlish</h3> </div> */}
        <div className="App firebaseui-auth-container" />
      
              <SignedIn id={this.unregisterAuthObserver}/>  
      </>
    );
  }
}



export default SignIn;