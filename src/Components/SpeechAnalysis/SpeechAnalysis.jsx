import React, { useState,useEffect } from 'react'
import axios from 'axios'
import 'react-dropzone-uploader/dist/styles.css'
import Dropzone from 'react-dropzone-uploader'
import { getDroppedOrSelectedFiles } from 'html5-file-selector'
import { Image ,Card,Icon,Label, Button,Header,Message } from 'semantic-ui-react'
import ModalExampleModal from './ModalTags'
const image_url  = "https://pixeyblob.blob.core.windows.net/630a961e-57b8-474d-a4a0-c3c089ccba99/uploads/test/download.jpeg?se=2021-09-24T17%3A58%3A58Z&sp=r&sv=2020-10-02&ss=b&srt=o&sig=2gm9/FraOl6YmhvdH1%2BlM09GcqqX3DJ2ohBtubBS/Zw%3D"
const ImageAudioVideo = () => {
    
    const getUploadParams = ({ file, meta ,name }) => {
        const data = new FormData()
        data.append('file', file)
        data.append('filename', name)

        fetch('http://localhost:8000/upload', { method: 'POST', body: data })
        .then((response) => { response.json().then((body) => { 
            console.log(response) 
          });
        });

        return { url: 'https://httpbin.org/post', data }
      }
  
    const handleChangeStatus = ({ meta }, status) => {
      console.log(status, meta)
    }
  
    const handleSubmit = (files, allFiles) => {
      console.log(files.map(f => f.meta))
      allFiles.forEach(f => f.remove())
    }
  
    return (

      <Dropzone
        getUploadParams={getUploadParams}
        onChangeStatus={handleChangeStatus}
        onSubmit={handleSubmit}
        accept="image/*,audio/*,video/*,.pdf"
        inputContent={(files, extra) => (extra.reject ? 'Image, audio and video files only' : 'Drag Files')}
        styles={{
          dropzoneReject: { borderColor: 'red', backgroundColor: '#DAA' },
          inputLabel: (files, extra) => (extra.reject ? { color: 'red' } : {}),
        }}
      />
    )
  }
  

const CardExampleCard = ({ OBJECT }) => {
    return(

        <Card>
        <Image src={OBJECT.url} wrapped ui={false} />
          <Card.Content>
              <Card.Meta>
              </Card.Meta>
              <Card.Description>
                  <Label.Group tag>
                      {
                          OBJECT.tags.split(" ").slice(0, 3).map((obj) =>
                                  <Label as='a' color='blue' >
                                      {obj}
                                  </Label>
                          )
                      }
                  </Label.Group>
              </Card.Description>
          </Card.Content>
              <Card.Content extra>
                  <ModalExampleModal objects= {OBJECT} />
                  {OBJECT.URL_MP4 &&
                    <Button content='Video' negative icon='download'  href={OBJECT.URL_MP4} />
                  }
                  { !OBJECT.URL_MP4 &&
                    <Button content='Image' negative icon='download'  href={OBJECT.url} />
                  }
                      
          </Card.Content>
      </Card>
  
    )
}
 
 
class SpeechAnalysis extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            imageURL: "",
            random_words_array:[]
        };
        this.handleUploadImage = this.handleUploadImage.bind(this);
    }
        
    
    async componentDidMount () {
        await axios.get( 'http://localhost:8000/all_images')
        .then(result => result.data).then(result => {
            this.setState({random_words_array:result} )})
            }

    handleUploadImage(ev) {
        ev.preventDefault();
        console.log(this.state.imageURL)
        const data = new FormData();
        data.append('file', this.uploadInput.files[0]);
        data.append('filename', this.fileName.value);
    
        fetch('http://localhost:8000/upload', { method: 'POST', body: data })
        .then((response) => { response.json().then((body) => { 
            console.log(response)
            console.log(`http://localhost:8000/${body.file}`)
            this.setState({ imageURL: `http://localhost:8000/${body.file}` });
          });
        });
      }
      
    render() {
        return (

            <div style={{backgroundColor:"#fafafa" ,position:"absolute" ,width:"95%"}}>
                <div className="addAppMainecontainer" style={{  marginTop:"5em",marginLeft:"1em",marginRight:"2em" }}>
                    <div className="androidApps" style={{height:"100%",width:"100%"}}>
                      <Header as='h2'>
                              <Icon name='database' />
                              <Header.Content> Repository</Header.Content>
                        </Header>
                        <ImageAudioVideo/>
                        <Message info>
                        <Message.Header>Image/Video/Document Uploader</Message.Header>
                          <p> Our Repository supports uploading of Image , Document and Video uploading format. Everything from here is directly pushed to azure storage </p>
                        </Message>
                      <br></br>
                    </div>
                    <Card.Group itemsPerRow={4}>
                        {
                        this.state.random_words_array.map( (obj) =>
                            <CardExampleCard 
                                    OBJECT={obj}
                            />
                            )
                        }
                    </Card.Group>
                </div> 
            </div>
        );
      }
}

export default SpeechAnalysis;

