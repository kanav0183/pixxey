import React from 'react'
import { Button, Card, Header, Label, Modal ,Icon,Image} from 'semantic-ui-react'
import axios from 'axios'

function ModalExampleModal({objects}) {
  const [open, setOpen] = React.useState(false)

  const LabelsEdit=({tag ,image_id ,doc_id})=>{
      
    const handleCorrectText =async() =>{ 

      await axios.get("http://localhost:8000/removetag",{params:{
            imageid:image_id ,removing_tag :tag ,doc_id:doc_id }}).then(result => result.data)
                } 

      return(
        <Label as='a' color='blue' onClick={handleCorrectText}>
            {tag}
            <Icon name='delete' />
        </Label>
      )
    }

  return (
    <Modal
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={open}
      trigger={<Button  positive fluid >More Tags</Button>}
    >
      <Modal.Header>Image details</Modal.Header>
        <Modal.Content image>
        <Image size='medium' src={objects.url} wrapped />

            <Modal.Description>
                <Header>Description: {objects.text}</Header>
                 <Card centered>  
                    <Label.Group  tag>
                                {
                                    objects.tags.split(" ").map( (obj) =>
                                            <LabelsEdit tag={obj} image_id={objects.imageid} doc_id={objects.id} />
                                            )
                                }
                    </Label.Group>
          </Card>
          
        </Modal.Description>
      </Modal.Content>
      <Modal.Actions>
        <Button color='black' onClick={() => setOpen(false)}>
          Nope
        </Button>
        <Button
          content="Yep, that's me"
          labelPosition='right'
          icon='checkmark'
          onClick={() => setOpen(false)}
          positive
        />
      </Modal.Actions>
    </Modal>
  )
}

export default ModalExampleModal