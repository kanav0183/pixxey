import React, { useState } from 'react'
import 'react-dropzone-uploader/dist/styles.css'
import Dropzone from 'react-dropzone-uploader'
import { Image ,Card,Label,Form,Button , Message }  from 'semantic-ui-react'
import ModalExampleModal from './ModalTags'
import axios from 'axios'


const CardExampleCard = ({ OBJECT }) => {
    return(

        <Card>
        <Image src={OBJECT.url} wrapped ui={false} />
          <Card.Content>
              <Card.Meta>
              </Card.Meta>
              <Card.Description>
                  <Label.Group tag>
                      {
                          OBJECT.tags.split(" ").slice(0, 3).map((obj) =>
                                  <Label as='a' color='blue' >
                                      {obj}
                                  </Label>
                          )
                      }
                  </Label.Group>
              </Card.Description>
          </Card.Content>
              <Card.Content extra>
                  <ModalExampleModal objects= {OBJECT} />
                      
          </Card.Content>
      </Card>
  
    )
}
 
 

const TextSearchImage =()=>{
    const [random_words_array ,setrandom_words_array ]= useState([])
    const [emailInput, setEmail] = useState("");
    const [Time, setTime] = useState(0);

    const handleSubmit= async ()=>{
        await axios.get("http://localhost:8000/text",{params:{
            text:emailInput}}).then(result => result.data).then(result => {
                setrandom_words_array(JSON.parse(result.datas))
                setTime(result.time)
            })
        } 
        console.log(random_words_array)
        return (

            <div style={{backgroundColor:"#fafafa" ,position:"absolute" ,width:"95%"}}>
                <div className="addAppMainecontainer" style={{  marginTop:"5em",marginLeft:"1em",marginRight:"2em" }}>
                    <div className="androidApps" style={{height:"100%",width:"100%"}}>
                    
                    <Message info>
                        <Message.Header>Text Search</Message.Header>
                          <p> Search images using text</p>
                    </Message>
                    <Form onSubmit ={handleSubmit} >
                        <Form.Input
                            icon='envelope'
                            iconPosition='left'
                            label='Text'
                            placeholder='Anything you want to search '
                            required
                            value={emailInput}
                            onChange={e => {
                                setEmail(e.target.value);                               
                            }}
                        />
                        <Button type="submit" primary>Submit</Button>
                    </Form>
                    <h>
                        Time  Taken = {Time}
                    </h>
                    </div>
                    <Card.Group itemsPerRow={4}>
                        {
                        random_words_array.map( (obj) =>
                            <CardExampleCard 
                                    OBJECT={obj}
                            />
                            )
                        }
                    </Card.Group>
                </div> 
            </div>
        );
      }


export  {TextSearchImage};

