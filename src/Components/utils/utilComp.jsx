import React from 'react'
import Loader from 'react-loader-spinner';
const LoaderCircles =() =>{
    return(
        <div
        style={{
        width: "100%",
        height: "100%",
        position: "fixed", /* or absolute */
        top: "50%",
        left: "46%",
        }}>
        <Loader type="Circles" color="#a8a497" height={120} width={120} />
    </div>
    )
}

export {LoaderCircles};